#
# ~/.bashrc
#

#Ibus settings if you need them
#type ibus-setup in terminal to change settings and start the daemon
#delete the hashtags of the next lines and restart
#export GTK_IM_MODULE=ibus
#export XMODIFIERS=@im=dbus
#export QT_IM_MODULE=ibus

# starship prompt
eval "$(starship init bash)"

PS1='[\u@\h \W]\$ '

# fzf keybindings
eval "$(fzf --bash)"

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

export HISTCONTROL=ignoreboth:erasedups

#ignore upper and lowercase when TAB completion
bind "set completion-ignore-case on"

######## Load aliases and shortcuts if existent.
[ -f "$HOME/scripts/shortcutrc" ] && source "$HOME/scripts/shortcutrc"
[ -f "$HOME/scripts/envrc" ] && source "$HOME/scripts/envrc"
[ -f "$HOME/scripts/aliasrc" ] && source "$HOME/scripts/aliasrc"

# reporting tools - install when not installed
# install neofetch
neofetch

